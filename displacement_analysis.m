%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Displacement analysis per state based on vbSPT (Persson et al.,
% 2013) results 
%
% Note: input is CellTracks (localisations (x, y coordinates) of
% trajectories) and CellTrackViterbiClass (vbSPT states of jumps of
% trajectories).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


[fn1 pt1] = uigetfile('*.mat'); % CellTracks    
[fn2 pt2] = uigetfile('*.mat'); % CellTrackViterbiClass
load(fn1);
load(fn2);

for state = 1:4
    k = 1;
    for i = 1:length(CellTracks)
        for j = 1:length(CellTracks{i})-1
            if CellTrackViterbiClass{i}(j)== state

            x1 = CellTracks{i}(j,1);
            x2 = CellTracks{i}(j+1,1);
            y1 = CellTracks{i}(j,2);
            y2 = CellTracks{i}(j+1,2);
        
            dist(k) = sqrt((x2-x1)^2+(y2-y1)^2);
            k = k+1;
            clear x1 x2 y1 y2
            end
        end
    end

distances{state} = dist/1000; % convert nm to μm
clear dist
end

figure

x1 = ones(1,length(distances{1}));
x2 = 2 * ones(1,length(distances{2}));
x3 = 3 * ones(1,length(distances{3}));
x4 = 4 * ones(1,length(distances{4}));

swarmchart(x1,distances{1},5)
hold on
swarmchart(x2,distances{2},5)
hold on
swarmchart(x3,distances{3},5)
hold on
swarmchart(x4,distances{4},5)

names = {'D1'; 'D2';'D3';'D4'};
set(gca,'xtick',[x1(1), x2(1), x3(1), x4(1)],'xticklabel',names)
