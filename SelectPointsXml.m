%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Crop a point pattern described as (..., x, y, type, *)
% Author: lam94@cam.ac.uk
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function trackid = SelectPointsXml(data, nuc)

n = 1 % several crops could be performed in a different setting. Here only 1
factor = 1;%0;
data(:,4:5) = data(:,4:5)/factor;
m= floor(max(data))+1;
for k=1:n
    figure
    clear id
    if ~isempty(nuc)
        h_im =imshow(nuc, []); hold on
    else
        h_im =imshow(zeros(m(5),m(4))); hold on
    end
    
%     if size(data,2)>2
%         scatter(data(:,2),data(:,3), data(:,4), data(:,3)); 
%     else
    scatter(data(:,4),data(:,5), 0.5,'c.'); 
%     end
    e = impoly(gca,'Closed',true); %rct{sel}([1 3 4 2 ]',:),
    BW = createMask(e,h_im);     
    id = find(BW(sub2ind(size(BW), floor(data(:,5))+1,floor(data(:,4))+1))>0);     
    plot(data(id,4),data(id,5), 'c.'); hold on 
    
    trackid = unique(data(id,1))
    % saveas(gcf, strcat(name, 'ROI',num2str(k),'.png'), 'png')
    % csvwrite(strcat(name, 'ROI',num2str(k),'.csv'),data(id,:) );
    % close all
    % dat = data(id,:);
end


