%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Control for anisotropy analysis (same analysis as in
% cluster_anisotropy_analysis but with clustered bound molecules randomly
% shifted in the nucleus)
%
% Note: input is txt files obtained from WriteTable and SeparateRegions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
 
clear all
close all
t = csvread('away_17_03_Notch_2.txt');
ClusterTable = readtable('Res_knn_11_away_17_03_Notch_2.txt');
name = 'Res_knn_11_away_17_03_Notch_2_550nmSEARCH';

%%
t(:,7)= abs(t(:,5))>150 & abs(t(:,5))<210;

[mm sds ss ll md] = grpstats(t,t(:,1),{'mean','std', 'sum', 'length', 'mode'}); %grpstats based on trajid t(:,1)

idD1D2 = find(md(:,2)<3); % bound tracks
idD3D4 = find(md(:,2)>2); % free/exploratory tracks
%%
ClusterTable = ClusterTable(:,2:end);
ClusterTable = table2array(ClusterTable);
idClust = find(ClusterTable(:,4)==2);

%%%%%% free molecules %%%%%%
xxFree = mm(idD3D4,3);
yyFree = mm(idD3D4,4);

BoundaryFreeOrig = convhull(xxFree,yyFree);
FreePolygon = polyshape(xxFree(BoundaryFreeOrig),yyFree(BoundaryFreeOrig));
% find upper and lower x,y shift thresholds using size of FreePolygon
[xCentre,yCentre] = centroid(FreePolygon)
HorizontalLine = line([xCentre-50000,xCentre+50000],[yCentre,yCentre]);
VerticalLine = line([xCentre,xCentre],[yCentre-50000,yCentre+50000]);
[xHorizontal,yHorizontal] = polyxpoly([xCentre-50000,xCentre+50000],[yCentre,yCentre],xxFree(BoundaryFreeOrig),yyFree(BoundaryFreeOrig));
[xVertical,yVertical] = polyxpoly([xCentre,xCentre],[yCentre-50000,yCentre+50000],xxFree(BoundaryFreeOrig),yyFree(BoundaryFreeOrig));

DHorizontal = pdist([xHorizontal,yHorizontal]);
DVertical = pdist([xVertical,yVertical]);

X_lower_SHIFT_threshold = DHorizontal/10;
X_upper_SHIFT_thershold = DHorizontal/2;

Y_lower_SHIFT_threshold = DVertical/10;
Y_upper_SHIFT_thershold = DVertical/2;

xxClust = ClusterTable(idClust,2);
yyClust = ClusterTable(idClust,3);

controlsObtained = 1;
for repeatNo = 1:100
    
    while controlsObtained <= 20

    %%%%%% clustered bound shifted %%%%%%
    xSHIFT = X_lower_SHIFT_threshold + (X_upper_SHIFT_thershold-X_lower_SHIFT_threshold) .* rand(1,1);
    PlusorMinus =  randperm(2);
    if PlusorMinus(1) == 2
       xSHIFT = -xSHIFT;
    end

    ySHIFT = Y_lower_SHIFT_threshold + (Y_upper_SHIFT_thershold-Y_lower_SHIFT_threshold) .* rand(1,1);
    PlusorMinus =  randperm(2);
    if PlusorMinus(1) == 2
       ySHIFT = -ySHIFT;
    end

    xxClustSHIFT = xxClust + xSHIFT;
    yyClustSHIFT = yyClust + ySHIFT;
    BoundaryClustSHIFT = convhull(xxClustSHIFT,yyClustSHIFT);

    ShiftedPolygon = polyshape(xxClustSHIFT(BoundaryClustSHIFT),yyClustSHIFT(BoundaryClustSHIFT));

    %%%%%% intersection of clustered bound orginal and clustered bound shifted %%%%%%
    FreeBoundShiftedIntersection = intersect(FreePolygon,ShiftedPolygon);

    %find which of shifted clustered are still in nucleus
    InOut = inpolygon(xxClustSHIFT,yyClustSHIFT,FreeBoundShiftedIntersection.Vertices(:,1),FreeBoundShiftedIntersection.Vertices(:,2));

    ProportionRemoved = length(find(InOut==0))/length(InOut);

    if ProportionRemoved < 0.2
       
       % plot to make sure it makes sense
       figure
       scatter(xxFree, yyFree, '.y')
       hold on
       plot(xxFree(BoundaryFreeOrig),yyFree(BoundaryFreeOrig),'-g','LineWidth',2)
       plot(xxClustSHIFT(BoundaryClustSHIFT),yyClustSHIFT(BoundaryClustSHIFT),'-r','LineWidth',2)
       plot(FreeBoundShiftedIntersection,'FaceColor','b')
       scatter(xxClustSHIFT(InOut), yyClustSHIFT(InOut), '.k')
       set(gca, 'YDir','reverse')
       ylim([0,Inf])

       % identify D3 D4 near clustered bound and plot
       [clusterid dcenter]= knnsearch([xxClustSHIFT(InOut), yyClustSHIFT(InOut)], t(:,3:4), 'K',1);
       ii = find((dcenter<550)&(t(:,2)>2));
       iii = find((dcenter>=550)&(t(:,2)>2));
       fig3 = figure;
       scatter(xxClustSHIFT(InOut), yyClustSHIFT(InOut), '.k'); hold on
       gscatter(t(ii,3), t(ii,4), clusterid(ii));
       legend off
       set(gca, 'YDir','reverse')

       AnisotropyAwayClust = sum(t(iii,7)==1)/length(iii);
       AnisotropyNearClust = sum(t(ii,7)==1)/length(ii);
       nAway = length(iii);
       nNear = length(ii);

       ResultMatrix(controlsObtained,:) = [AnisotropyAwayClust,AnisotropyNearClust,nAway,nNear,xSHIFT,ySHIFT];
       controlsObtained = controlsObtained + 1;
    end

clear xSHIFT ySHIFT xxClustSHIFT yyClustSHIFT BoundaryClustSHIFT ...
    ShiftedPolygon FreeBoundShiftedIntersection InOut ProportionRemoved ...
    clusterid dcenter ii iii AnisotropyAwayClust AnisotropyNearClust ...
    nAway nNear
    end
    
end

save(['CONTROL_',name,'.mat'],'ResultMatrix')

    
