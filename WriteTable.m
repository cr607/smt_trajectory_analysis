%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prepares data for nnclean analysis in R
%
% Note: input is CellTracks (localisations (x, y coordinates) of
% trajectories) and CellTrackViterbiClass (vbSPT states of jumps of
% trajectories)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[fn1 pt1] = uigetfile('*.mat'); % CellTracks    
[fn2 pt2] = uigetfile('*.mat'); % CellTrackViterbiClass
load(fn1);
load(fn2);

t = double([0 0 0 0 0 0]);
for k = 1:length(CellTracks_away)
    t = double([t; double(k*ones(size(angle_degrees{k}(:,2)))) double(CellTrackViterbiClass_away{k}(2:end)) CellTracks_away{k}(2:end-1,1),CellTracks_away{k}(2:end-1,2), angle_degrees{k}(:,1), angle_degrees{k}(:,2)]);
end
%% save table
csvwrite('nuclear_SuH_6.txt', t);
