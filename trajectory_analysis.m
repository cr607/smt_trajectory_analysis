%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Analyse vbSPT results. Includes: population proportion analysis
% near and away the locus, angle analysis, state transition analysis,
% asymmetry and directionality analyses

% Note: requires vbSPT function VB3_readData (Persson et al.
% 2013)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear all
close all
global minTraj 
minTraj  = 2;

%% make sure you are in the parent folder of the results folder
px = 110;    % pixel size

[fn1 pt1] = uigetfile('*.mat', 'Select vbSPT output file'); %vbSPT output
%%
[fn2 pt2] = uigetfile('*.tif', 'Select locus image'); %locus image
im = imread(fullfile(pt2, fn2));
original_image = imread(fn2);
figure
imshow(original_image, []) %plot original image

%% read in vbspt result
a = load(fullfile(pt1, fn1));

% Read in the trajectory data
a.options.dim = 2;
D_states = a.Wbest.est.DdtMean/a.options.timestep;
X = VB3_readData(a.options);
traj_state = a.Wbest.est2.sMaxP;
start = 1;
stop = length(X);
longTrajs = [];

% Loop over trajectories longer than minTraj
trjColoring = '<d(t)>';
lineWidth=1;
lg = cellfun(@length, X);
qmode = cellfun(@mode, traj_state);
counts = cellfun(@length,traj_state);
qmode(counts<2) = [];

q = zeros(stop-start+1,5);
for i = start:stop
    q(i,:) = [X{i}(1,1),X{i}(1,2),X{i}(end,1)-X{i}(1,1),X{i}(end,2)-X{i}(1,2),double(size(X{i},1))];
end

%% detect locus
%image thresholding using Otsus method (multithresh)
%a 2-D Gaussian smoothing kernel is used for filtering (imgaussfilt)
sigma1 = 4; %standard deviation of 2-D Gaussian smoothing kernel  
threshold_no = 2;
threshold_used = 2;
 
t = multithresh(im-imgaussfilt(im,sigma1),threshold_no);
figure; imagesc(im-imgaussfilt(im,sigma1)>t(threshold_used))
plane = im-imgaussfilt(im,sigma1)>t(threshold_used);
[ly lx] = find(plane);

% Set locus coordinates
x_1 = 160; x_2 = 280; 
y_1 = 75; y_2 = 200;

% remove non-locus pixels
lx_keep_id = find(lx>x_1 & lx<x_2);
ly_keep_id = find(ly>y_1 & ly<y_2);
l_keep_id = intersect(lx_keep_id,ly_keep_id);

lx = lx(l_keep_id);
ly = ly(l_keep_id);

% plot locus
plane = logical(zeros(size(plane)));
for i = 1:length(ly)
    plane(ly(i),lx(i))=logical(1);
end
figure;imagesc(plane)

xlim([x_1,x_2]);
ylim([y_1,y_2]);

rowNames = {'sigma1','threshold_no','threshold_used'};
colNames = {'Value'};
T0 = array2table([sigma1;threshold_no; threshold_used],'RowNames',rowNames,...
    'VariableNames',colNames);
writetable(T0,'locus_thresholds.csv','WriteRowNames',true)

rowNames = {'x1','x2','y1','y2'};
colNames = {'Value'};
T01 = array2table([x_1;x_2;y_1;y_2],'RowNames',rowNames,'VariableNames',colNames);
writetable(T01,'locus_coordinates.csv','WriteRowNames',true)
%% plot trajectories
fig1 = figure;
C = {'-c','-g','-r','-m'};
traj_id = cell(1,4);
state_fraction = zeros(1,4);
for i =1:4
    traj_id{i} = find(qmode==i);
    subplot(2,2,i)
%      imagesc(plane)
    hold on
    for k = 1:length(traj_id{i}) 
        plot(X{traj_id{i}(k)}(:,1)/px,X{traj_id{i}(k)}(:,2)/px, C{i}); hold on
    end 
    state_fraction(i) = length(traj_id{i})/length(X);
    title(['D',sprintf('%d',i),' ',sprintf('%d',state_fraction(i))])
end 
saveas(fig1,'trajectories_per_state.png')

fig2 = figure;
traj_id = cell(1,4);
state_fraction = zeros(1,4);
for i =1:4
    traj_id{i} = find(qmode==i);
    subplot(2,2,i)
    imagesc(plane)
    hold on
    for k = 1:length(traj_id{i}) 
        plot(X{traj_id{i}(k)}(:,1)/px,X{traj_id{i}(k)}(:,2)/px, C{i}); hold on
    end 
    state_fraction(i) = length(traj_id{i})/length(X);
    title(['D',sprintf('%d',i),' ',sprintf('%d',state_fraction(i)),'near locus'])
    xlim([x_1,x_2]);
    ylim([y_1,y_2]);        
end 
saveas(fig2,'trajectories_per_state_locus_zoom.png')

%% identify tracks near/away based on their starting position
d = bwdist(plane); %computes the Euclidean distance transform of the binary image 
%for each pixel, the distance transform assigns a number that is the distance between that pixel and the nearest
%nonzero pixel of the image.
startpos = [];
endpos = [];
sd = [];
for k = 1:size(q,1)
    startpos(k) = d(max(1,floor(q(k,2)/px)),max(1,floor(q(k,1)/px))); %note that the order of x and y must be reversed
    endpos(k) = d(max(1,floor((q(k,2)+q(k,4))/px)),max(1,floor((q(k,1)+q(k,3))/px)) );
    %signed distance 
        % - positive dist. motion away from locus
        % - negative dist. motion towards from locus
    sd(k) =endpos(k)-startpos(k);    
end

radius_size = [5];
id = {};
not_id = {};
for i = 1:length(radius_size)
    id{i} = find(startpos <= radius_size(i)|endpos <= radius_size(i)); %near locus
    
    trajectory_id = [1:size(q,1)];
    idx = ismember(trajectory_id, id{i});
    not_id{i} = trajectory_id(~idx); %away from locus
end

%% plot near locus trajectories
fig3 = figure; 
track_length = cell(1,length(radius_size));

for i = 1:length(radius_size)
    track_length{i} = zeros(1,length(id{i}));  
    subplot(1,length(radius_size),i)
    imagesc(plane)
    hold on
    for k = 1:length(id{i})    
        track_length{i}(k) = length(X{id{i}(k)}(:,1));
        plot(X{id{i}(k)}(:,1)/px,X{id{i}(k)}(:,2)/px, '-'); hold on
    end
    %Change limits so that you can see locus!
    xlim([x_1,x_2]);
    ylim([y_1,y_2]);
    title({['Near locus trajectories ',sprintf('%d',radius_size(i)),'px']});
    hold off
end

saveas(fig3,'near_locus_trajectories.png')

%% plot away from locus trajectories
% If you want to plot 'away from locus trajectories uncomment all of the following
% lines:
 fig4 = figure; 
 track_length_not_id = cell(1,length(radius_size));
 for i = 1:length(radius_size)
     track_length_not_id{i} = zeros(1,length(not_id{i}));  
     subplot(1,length(radius_size),i)
     imagesc(plane)
     hold on
     for k = 1:length(not_id{i})    
         track_length_not_id{i}(k) = length(X{not_id{i}(k)}(:,1));
         plot(X{not_id{i}(k)}(:,1)/px,X{not_id{i}(k)}(:,2)/px, '-'); hold on
     end
     title({['Away locus trajectories ',sprintf('%d',radius_size(i)),'px']});
     hold off
 end
 
saveas(fig4,'away_locus_trajectories.png')

%% state occupancies near locus per track
total_states_near = cell(1,length(radius_size));
total_sum_near = cell(1,length(radius_size));
occupancy_near = cell(1,length(radius_size));
state_one_tracks = cell(1,length(radius_size));
state_two_tracks = cell(1,length(radius_size));
state_three_tracks = cell(1,length(radius_size));
state_four_tracks = cell(1,length(radius_size));

for i = 1:length(radius_size)
    state_one_tracks{i} = zeros(1,length(id{i}));
    state_two_tracks{i} = zeros(1,length(id{i}));
    state_three_tracks{i} = zeros(1,length(id{i}));
    state_four_tracks{i} = zeros(1,length(id{i}));
    
    for j = 1:length(id{i}) 
        state_one_tracks{i}(j) = sum(traj_state{id{i}(j)}(:)==1);
        state_two_tracks{i}(j) = sum(traj_state{id{i}(j)}(:)==2);
        state_three_tracks{i}(j) = sum(traj_state{id{i}(j)}(:)==3);
        state_four_tracks{i}(j) = sum(traj_state{id{i}(j)}(:)==4);  
    end
    
    total_states_near{i} = [sum(state_one_tracks{i});sum(state_two_tracks{i});...
    sum( state_three_tracks{i});sum( state_four_tracks{i})];
    total_sum_near{i} = sum(total_states_near{i});
    occupancy_near{i} = total_states_near{i}/total_sum_near{i};    
end

%% state occupancies away locus per track
total_states_away = cell(1,length(radius_size));
total_sum_away = cell(1,length(radius_size));
occupancy_away = cell(1,length(radius_size));
state_one_away_tracks = cell(1,length(radius_size));
state_two_away_tracks = cell(1,length(radius_size));
state_three_away_tracks = cell(1,length(radius_size));
state_four_away_tracks = cell(1,length(radius_size));

for i = 1:length(radius_size)
    
    state_one_away_tracks{i} = zeros(1,length(id{i}));
    state_two_away_tracks{i} = zeros(1,length(id{i}));
    state_three_away_tracks{i} = zeros(1,length(id{i}));
    state_four_away_tracks{i} = zeros(1,length(id{i}));

    for j = 1:length(not_id{i})
        state_one_away_tracks{i}(j) = sum(traj_state{not_id{i}(j)}(:)==1);
        state_two_away_tracks{i}(j) = sum(traj_state{not_id{i}(j)}(:)==2);
        state_three_away_tracks{i}(j) = sum(traj_state{not_id{i}(j)}(:)==3);
        state_four_away_tracks{i}(j) = sum(traj_state{not_id{i}(j)}(:)==4);
    end
    total_states_away{i} = [sum(state_one_away_tracks{i});...
    sum(state_two_away_tracks{i});sum(state_three_away_tracks{i});...
    sum(state_four_away_tracks{i})];
    total_sum_away{i} = sum(total_states_away{i});
    occupancy_away{i} = total_states_away{i}/total_sum_away{i};  

    rowNames = {'D1','D2','D3','D4'};
    colNames = {['Jumps_near_',sprintf('%d',radius_size(i)),'px'],...
    ['Jumps_away_',sprintf('%d',radius_size(i)),'px'],...
    ['Occupancy_near_',sprintf('%d',radius_size(i)),'px'],...
    ['Occupancy_away_',sprintf('%d',radius_size(i)),'px']};
    T1 = array2table([total_states_near{i},total_states_away{i},...
    occupancy_near{i},occupancy_away{i}],'RowNames',rowNames,...
    'VariableNames',colNames);
    disp(T1)
    writetable(T1,'jumps_occupancies_near_away.csv','WriteRowNames',true)

end

%% state occupancy per trajectory
total_trajectories_near = cell(1,length(radius_size));
trajectory_occupancy_near = cell(1,length(radius_size));
total_trajectories_away = cell(1,length(radius_size));
trajectory_occupancy_away = cell(1,length(radius_size));

for i = 1 : length(radius_size)
    total_trajectories_near{i} = [sum(qmode(id{i})==1);...
      sum(qmode(id{i})==2);sum(qmode(id{i})==3);...
      sum(qmode(id{i})==4)];
    trajectory_occupancy_near{i} = total_trajectories_near{i}/length(id{i}); 
   
     total_trajectories_away{i} = [sum(qmode(not_id{i})==1);...
      sum(qmode(not_id{i})==2);sum(qmode(not_id{i})==3);...
      sum(qmode(not_id{i})==4)];
     trajectory_occupancy_away{i} = total_trajectories_away{i}/length(not_id{i}); 
     
    rowNames = {'D1','D2','D3','D4'};
    colNames = {['Trajectories_near_',sprintf('%d',radius_size(i)),'px'],...
    ['Trajectories_away_',sprintf('%d',radius_size(i)),'px'],...
    ['Occupancy_near_',sprintf('%d',radius_size(i)),'px'],...
    ['Occupancy_away_',sprintf('%d',radius_size(i)),'px']};

    T2 = array2table([ total_trajectories_near{i},total_trajectories_away{i},...
        trajectory_occupancy_near{i},trajectory_occupancy_away{i}],'RowNames',rowNames,...
        'VariableNames',colNames);
    disp(T2)
    writetable(T2,'trajectory_occupancies_near_away.csv','WriteRowNames',true)

end
%% investigate angles near locus
for i = 1 : length(radius_size)
    data{i} = zeros(100000,3);

    for j = 1:length(id{i})
        S = sum(track_length{i}(1:j));
        if track_length{i}(j) >= 3
            for k = 2 : track_length{i}(j)-1
            
                s1 = traj_state{id{i}(j)}(k-1);
            	s2 = traj_state{id{i}(j)}(k);
  
                x1 = X{id{i}(j)}(k-1,1);
                x2 = X{id{i}(j)}(k,1);
                x3 = X{id{i}(j)}(k+1,1);
   
                y1 = X{id{i}(j)}(k-1,2);
                y2 = X{id{i}(j)}(k,2);
                y3 = X{id{i}(j)}(k+1,2);
            
     
                u = [x2-x1 y2-y1];
                v = [x3-x2 y3-y2];
        
                CosTheta =(dot(u,v)/(norm(u)*norm(v)));
                angle = real(acos(CosTheta) * 180/pi);
            
            
                line_grad = u(2)/u(1);
                line_intercept = y1 - line_grad * x1;
                if line_grad>0 && y3 > (line_grad*x3 + line_intercept)...
                        || line_grad < 0 &&  y3 < (line_grad*x3 +  line_intercept)
                    angle = 360 - angle;
                else
                end
            
                data{i}(k+S,1) = s1;
                data{i}(k+S,2) = s2;
                data{i}(k+S,3) = angle;          
            end
        else
        end
    end
    
    data{i}(~any(data{i},2),:) = []; %delete zero rows
    data{i} = rmmissing(data{i}); %delete zero rows
end
%% investigate angles away from locus
for i = 1 : length(radius_size)
    data_away{i} = zeros(100000,3);
    for j = 1:length(not_id{i})
        S = sum(track_length_not_id{i}(1:j));
        if track_length_not_id{i}(j) >= 3
            for k = 2 : track_length_not_id{i}(j)-1
        
                s1 = traj_state{not_id{i}(j)}(k-1);
                s2 = traj_state{not_id{i}(j)}(k);
        
                x1 = X{not_id{i}(j)}(k-1,1);
                x2 = X{not_id{i}(j)}(k,1);
                x3 = X{not_id{i}(j)}(k+1,1);
   
                y1 = X{not_id{i}(j)}(k-1,2);
                y2 = X{not_id{i}(j)}(k,2);
            	y3 = X{not_id{i}(j)}(k+1,2);
            
                u = [x2-x1 y2-y1];
                v = [x3-x2 y3-y2];
        
                %CosTheta = max(min(dot(u,v)/(norm(u)*norm(v)),1),-1);
                %angle = real(acosd(CosTheta));
            
                CosTheta =(dot(u,v)/(norm(u)*norm(v)));
                angle = real(acos(CosTheta) * 180/pi);
            
                line_grad = u(2)/u(1);
                line_intercept = y1 - line_grad * x1;
                    if line_grad>0 && y3 > (line_grad*x3 + line_intercept)...
                        || line_grad < 0 &&  y3 < (line_grad*x3 +  line_intercept)
                        angle = 360 - angle;
                    else
                    end
            
                data_away{i}(k+S,1) = s1;
                data_away{i}(k+S,2) = s2;
                data_away{i}(k+S,3) = angle;            
          
            end
        else
            
        end
    end 


    data_away{i}(~any(data_away{i},2),:) = []; %delete zero rows  
    data_away{i} = rmmissing(data_away{i}); %delete zero rows  
end 
%% plot angles
for i = 1 : length(radius_size)

    fig5 = figure
    for k = 1 : 4
        
        data_k = data{i}((data{i}(:,1)==k & data{i}(:,2)==k),:);
        subplot(2,2,k);
        rose(data_k(:,3)*pi/180)
        title({['Near locus D', sprintf('%d',k)],['Threshlod distance ',sprintf('%d',radius_size(i)),'px'] }, 'FontSize', 10);
        clear data_k
    end   
    saveas(fig5,'angles_near.png')

    
    fig6 = figure
    for k = 1 : 4
            
        data_away_k = data_away{i}((data_away{i}(:,1)==k & data_away{i}(:,2)==k),:);
        subplot(2,2,k);
        rose(data_away_k(:,3)*pi/180)
        title({['Away locus D', sprintf('%d',k)],['Threshlod distance ',sprintf('%d',radius_size(i)),'px'] }, 'FontSize', 10);
        clear data_away_k
    end  
    saveas(fig6,'angles_away.png')

end

%% count state transitions
for i = 1:length(radius_size) 
    ct_ii_i = 1;
    ct_i_ii = 1;
    two_to_one_transition = zeros(1,1000);%near locus
    one_to_two_transition = zeros(1,1000);
    for j = 1: length(id{i})
        for k = 1:length(traj_state{id{i}(j)})-1
               if traj_state{id{i}(j)}(k)==2 && traj_state{id{i}(j)}(k+1)==1
                  two_to_one_transition(ct_ii_i) = 1;
                  ct_ii_i = ct_ii_i + 1;
               elseif traj_state{id{i}(j)}(k)== 1 && traj_state{id{i}(j)}(k+1)== 2
                  one_to_two_transition(ct_i_ii) = 1;
                  ct_i_ii = ct_i_ii + 1;
               else
               end
        end
    end
    total_two_to_one = sum(two_to_one_transition);
    total_one_to_two = sum(one_to_two_transition);

    ct_ii_i = 1;
    ct_i_ii = 1;
    two_to_three_transition = zeros(1,1000);
    three_to_two_transition = zeros(1,1000);
    for j = 1: length(id{i})
        for k = 1:length(traj_state{id{i}(j)})-1
               if traj_state{id{i}(j)}(k)==2 && traj_state{id{i}(j)}(k+1)==3
                  two_to_three_transition(ct_ii_i) = 1;
                  ct_ii_i = ct_ii_i + 1;
               elseif traj_state{id{i}(j)}(k)== 3 && traj_state{id{i}(j)}(k+1)== 2
                  three_to_two_transition(ct_i_ii) = 1;
                  ct_i_ii = ct_i_ii + 1;
               else
               end
        end
    end
    total_two_to_three = sum(two_to_three_transition);
    total_three_to_two = sum(three_to_two_transition);

    ct_iii_i = 1;
    ct_i_iii = 1;
    one_to_three_transition = zeros(1,1000);
    three_to_one_transition = zeros(1,1000);
    for j = 1: length(id{i})
        for k = 1:length(traj_state{id{i}(j)})-1
               if traj_state{id{i}(j)}(k)==1 && traj_state{id{i}(j)}(k+1)==3
                  one_to_three_transition(ct_iii_i) = 1;
                  ct_iii_i = ct_iii_i + 1;
               elseif traj_state{id{i}(j)}(k)== 3 && traj_state{id{i}(j)}(k+1)== 1
                  three_to_one_transition(ct_i_iii) = 1;
                  ct_i_iii = ct_i_iii + 1;
               else
               end
        end
    end
    total_one_to_three = sum(one_to_three_transition);
    total_three_to_one = sum(three_to_one_transition);

    ct_ii_i = 1;
    ct_i_ii = 1;

    two_to_one_transition_away = zeros(1,1000);
    one_to_two_transition_away = zeros(1,1000);
    for j = 1: length(not_id{i}) %away locus
        for k = 1:length(traj_state{not_id{i}(j)})-1
               if traj_state{not_id{i}(j)}(k)==2 && traj_state{not_id{i}(j)}(k+1)==1
                  two_to_one_transition_away(ct_ii_i) = 1;
                  ct_ii_i = ct_ii_i + 1;
               elseif traj_state{not_id{i}(j)}(k)== 1 && traj_state{not_id{i}(j)}(k+1)== 2
                  one_to_two_transition_away(ct_i_ii) = 1;
                  ct_i_ii = ct_i_ii + 1;
               else
               end
        end
    end
    total_two_to_one_away = sum(two_to_one_transition_away);
    total_one_to_two_away = sum(one_to_two_transition_away);                
                   
    ct_iii_i = 1;
    ct_i_iii = 1;
    one_to_three_transition_away = zeros(1,1000);
    three_to_one_transition_away = zeros(1,1000);
    for j = 1: length(not_id{i})
        for k = 1:length(traj_state{not_id{i}(j)})-1
               if traj_state{not_id{i}(j)}(k)==3 && traj_state{not_id{i}(j)}(k+1)==1
                  three_to_one_transition_away(ct_iii_i) = 1;
                  ct_iii_i = ct_iii_i + 1;
               elseif traj_state{not_id{i}(j)}(k)== 1 && traj_state{not_id{i}(j)}(k+1)== 3
                  one_to_three_transition_away(ct_i_iii) = 1;
                  ct_i_iii = ct_i_iii + 1;
               else
               end
        end
    end
    total_three_to_one_away = sum(three_to_one_transition_away);
    total_one_to_three_away = sum(one_to_three_transition_away);    

    ct_iii_ii = 1;
    ct_ii_iii = 1;
    
    two_to_three_transition_away = zeros(1,1000);
    three_to_two_transition_away = zeros(1,1000);
    for j = 1: length(not_id{i})
        for k = 1:length(traj_state{not_id{i}(j)})-1
               if traj_state{not_id{i}(j)}(k)==3 && traj_state{not_id{i}(j)}(k+1)==2
                  three_to_two_transition_away(ct_iii_ii) = 1;
                  ct_iii_ii = ct_iii_ii + 1;
               elseif traj_state{not_id{i}(j)}(k)== 2 && traj_state{not_id{i}(j)}(k+1)== 3
                  two_to_three_transition_away(ct_ii_iii) = 1;
                  ct_ii_iii = ct_ii_iii + 1;
               else
               end
        end
    end
    total_three_to_two_away = sum(three_to_two_transition_away);
    total_two_to_three_away = sum(two_to_three_transition_away);    


    for j = 1: length(id{i})
        M_near(j) = length(X{id{i}(j)})-1;
    end
    for j = 1: length(not_id{i})
        M_away(j) = length(X{not_id{i}(j)})-1;
    end
    total_number_tracks = sum(M_near)+sum(M_away);

    fraction_one_to_two_near_locus = total_one_to_two/total_number_tracks;
    fraction_two_to_one_near_locus = total_two_to_one/total_number_tracks;

    fraction_one_to_two_away_locus = total_one_to_two_away/total_number_tracks;
    fraction_two_to_one_away_locus = total_two_to_one_away/total_number_tracks;

    state_transition_matrix = [[total_two_to_one,total_two_to_one_away];...
    [total_one_to_two,total_one_to_two_away];[total_three_to_one,total_three_to_one_away];...
    [total_one_to_three,total_one_to_three_away];[total_three_to_two,total_three_to_two_away];...
    [total_two_to_three,total_two_to_three_away]];
    rowNames = {'D2_D1','D1_D2','D3_D1', 'D1_D3','D3_D2','D2_D3'};
    colNames = {['Near_locus_transitions_',sprintf('%d',radius_size(i)),'px'],['Away_locus_transitions_',sprintf('%d',radius_size(i)),'px']};
    T3 = array2table(state_transition_matrix,'RowNames',rowNames,'VariableNames',colNames);
    disp(T3)
    writetable(T3,'near_locus_transitions.csv','WriteRowNames',true)

    state_transition_fractions = [[fraction_two_to_one_near_locus,fraction_two_to_one_away_locus];[fraction_one_to_two_near_locus,fraction_one_to_two_away_locus]];               
    rowNames = {'D2_D1','D1_D2'};
    colNames = {['Fraction_near_locus_',sprintf('%d',radius_size(i)),'px'],['Fraction_away_locus_',sprintf('%d',radius_size(i)),'px']};
    T4 = array2table(state_transition_fractions,'RowNames',rowNames,'VariableNames',colNames);
    disp(T4)
    writetable(T4,'away_locus_transitions.csv','WriteRowNames',true)

end
%% asymmetry analysis
for i = 1:length(radius_size) 
    [a2_near{i} A2_near{i}] = cellfun(@TrajAssym,X(id{i})); %asymmetries for near locus trajectories
    [a2_away{i} A2_away{i}] = cellfun(@TrajAssym,X(not_id{i})); %asymmetries for near locus trajectories
    id_a2_near{i} = ~isnan(a2_near{i});
    id_a2_away{i} = ~isnan(a2_away{i});
end

%% plot asymmetries for each state near and away from locus
for i = 1:length(radius_size) 
    fig7 = figure
    for k = 1:max(unique(qmode))
        id_state_near = intersect(find(qmode==k),id{i});
        id_state_away = intersect(find(qmode==k),not_id{i});

        [a2_near_per_state A2_near_per_state] = cellfun(@TrajAssym,X(id_state_near));
        [a2_away_per_state A2_away_per_state] = cellfun(@TrajAssym,X(id_state_away));

    
    
        subplot(2,2,double(k))
        cumulative_plot_near = cdfplot(A2_near_per_state);%, 10); %choose the asymmetry to plot
        hold on
        cumulative_plot_away = cdfplot(A2_away_per_state);%, 20); %choose the asymmetry to plot

        set(cumulative_plot_near,'LineStyle', '-', 'Color', 'r');
        set(cumulative_plot_away,'LineStyle', '-', 'Color', 'b');

    
        title(strcat(['Asymmetries near and away locus D=',num2str(D_states(k)/1000000,2),', ',sprintf('%d',radius_size(i)), 'px']))
        xlabel('A2')%name the asymmetry that you plot
        legend('near','away')
        clear id_state_near a2_near_per_state A2_near_per_state cumulative_plot_near
    end
    saveas(fig7,'asymmetries.png')
end

%% plot directionality for D1 and D2
col = colormap('lines');
fig8 = figure
state_dir_1 = find(qmode==1);

imagesc(im-imgaussfilt(im,sigma1)>t(threshold_used));
hold on
quiver(q(state_dir_1,1)/px,q(state_dir_1,2)/px,q(state_dir_1,3)/px,q(state_dir_1,4)/px,'LineWidth',1.5, 'AutoScale','off', 'Color', col(2,:), 'LineWidth',1.5, 'AutoScale','off');  hold on
alpha(0.2);
legend('D1')

xlim([x_1,x_2]);
ylim([y_1,y_2]);
hold off
saveas(fig8,'directionality_D1.png')

fig9 = figure
state_dir_2= find(qmode==2);
imagesc(im-imgaussfilt(im,sigma1)>t(threshold_used));
hold on
quiver(q(state_dir_2,1)/px,q(state_dir_2,2)/px,q(state_dir_2,3)/px,q(state_dir_2,4)/px,'LineWidth',1.5, 'AutoScale','off', 'Color', col(1,:), 'LineWidth',1.5, 'AutoScale','off');  hold on
alpha(0.2);
legend('D2')

xlim([x_1,x_2]);
ylim([y_1,y_2]);
hold off
saveas(fig9,'directionality_D2.png')

clear spos epos sdist
%state by state
lengthThresh = 5;% length of trajectory
distThresh = radius_size;

for i = 1:length(distThresh)
    
    for s = max(unique(qmode)):-1:1
        inear = find((qmode==s)&(lg>lengthThresh));
        ifar = find(startpos(inear)>distThresh(i));
        inear(ifar) = [];
        [idx spos{s}] = knnsearch([ly lx],[q(inear,2)/px q(inear,1)/px]);
        [idx epos{s}] = knnsearch([ly lx],[(q(inear,2)+q(inear,4))/px (q(inear,1)+q(inear,3))/px]);
        sdist{s} = epos{s}-spos{s};    
    end

    skewness(sdist{1});
    skewness(sdist{2});
    skewness(sdist{3});
    median(sdist{1});
    median(sdist{2});
    median(sdist{3});
    rowNames = {'skewness','median'};
    colNames = {'D1','D2'};
    directionality_table_1 = array2table([[skewness(sdist{1}),skewness(sdist{2})];[median(sdist{1}),median(sdist{2})]],'RowNames',rowNames,'VariableNames',colNames);
    disp(['Threshold distance = ' num2str(radius_size(i)),'px'])
    disp(directionality_table_1)
    writetable(directionality_table_1,'directionality_table_1.csv','WriteRowNames',true)

    % statistical test per state. Comparing the mean to 0
    [h(1),pvalue(1),ci] = ttest(sdist{1},0); 
    [h(2),pvalue(2),ci] = ttest(sdist{2},0);
    [h(3),pvalue(3),ci] = ttest(sdist{3},0);

    rowNames = {'significance at 5%','p-value'};
    colNames = {'D1','D2'};
    directionality_table_2 = array2table([[h(1),h(2)];[pvalue(1),pvalue(2)]],'RowNames',rowNames,'VariableNames',colNames);
    disp(directionality_table_2)
    writetable(directionality_table_2,'directionality_table_2.csv','WriteRowNames',true)
end
