%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Angular statistics per state based on vbSPT (Persson et al.
% 2013) results 
%
% Note: input is CellTracks (localisations (x, y coordinates) of
% trajectories) and CellTrackViterbiClass (vbSPT states of jumps of
% trajectories).
% Requires Circular Statistics Toolbox, 
% P. Berens, CircStat: A Matlab Toolbox for Circular Statistics, 
% Journal of Statistical Software, Volume 31, Issue 10, 2009
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[fn1 pt1] = uigetfile('*.mat'); % CellTracks    
[fn2 pt2] = uigetfile('*.mat'); % CellTrackViterbiClass
load(fn1);
load(fn2);

counts = cellfun(@length,CellTracks);
CellTracks(counts<3) = [];
CellTrackViterbiClass(counts<2) = [];

angles = cell(1,length(CellTracks));

for i = 1 : length(CellTracks)
    for k = 2 : counts(i)-1
        x1 = CellTracks{i}(k-1,1);
        x2 = CellTracks{i}(k,1);
        x3 = CellTracks{i}(k+1,1);
   
        y1 = CellTracks{i}(k-1,2);
        y2 = CellTracks{i}(k,2);
        y3 = CellTracks{i}(k+1,2);
        
        Distances{i}(k-1,1:2) = [pdist([[x1,y1]; [x2,y2]]) pdist([[x2,y2]; [x3,y3]])];

        u = [x2-x1 y2-y1];
        v = [x3-x2 y3-y2];  
        
        CosTheta =(dot(u,v)/(norm(u)*norm(v)));
        angle = real(acos(CosTheta) * 180/pi);
                 
        line_grad = u(2)/u(1);
        line_intercept = y1 - line_grad * x1;
        if line_grad>0 && y3 > (line_grad*x3 + line_intercept)...
           || line_grad < 0 &&  y3 < (line_grad*x3 +  line_intercept)
           angle = 360 - angle;
        else
        end
        
        %convert in radians
        angles{i}(k-1,1) = angle * pi/180;  
        angles{i}(k-1,2) = CellTrackViterbiClass{i}(k);
    end
end

l = 0;
for i = 1 : length(angles)
    angle_data(1+l:l+length(angles{i}(:,1)),1:2) = angles{i};
    l = length(angle_data);
end

angle_data(find(isnan(angle_data(:,1))),:)=[];

for i = 1:4
    id{i} = find(angle_data(:,2)==i);
end

state_one_angles = angle_data(id{1},1);
state_two_angles = angle_data(id{2},1);
state_three_angles = angle_data(id{3},1);
state_four_angles = angle_data(id{4},1);


save(['state_one_angles_',name,'.mat'],'state_one_angles')
save(['state_two_angles_',name,'.mat'],'state_two_angles')
save(['state_three_angles_',name,'.mat'],'state_three_angles')
save(['state_four_angles_',name,'.mat'],'state_four_angles')

clear all
 
%load data 
[fn1 pt1] = uigetfile('*.mat', 'angle file');
load(fn1);
angle_data = state_one_angles; % change this depending what state you want to analyse

%%%%%%% PLOTS %%%%%%%
%% Plot the data
figure                                 
subplot(1,2,1)
circ_plot(angle_data,'pretty','ro',true,'linewidth',2,'color','r');
%title('pretty plot style')

subplot(1,2,2)
figure
circ_plot(angle_data,'hist',[],20,true,true,'linewidth',2,'color','r');
figure
circ_plot_rlim(angle_data,'hist',[],20,true,true,'linewidth',2,'color','r');
%title('hist plot style')

%% Descriptive statistics
descriptive_statistics = circ_stats(angle_data)

R = circ_r(angle_data)

%% Statistical test for uniformity
% Null hypothesis: The population is distributed uniformly around the circle
% a small p indicates a significant departure from uniformity and indicates to reject the 
% null hypothesis.

%%%%%%%%%%%%% Rayleigh test %%%%%%%%%%%%%%%%%%%%%%%%
% particularly suited for detecting a unimodal deviation from uniformity. 
% If the data indeed is unimodal, it is the most powerful test
% Importantly, it assumes sampling from a von Mises distribution.
p_Rayleigh = circ_rtest(angle_data)

%%%%%%%%%%%% Omnibus test %%%%%%%%%%%%%%%%%%
% alternative to the Rayleigh test that works well for unimodal, biomodal and multimodal distributions. 
% It is able to detect general deviations from uniformity at the price of some statistical power. 
% Also, it does not make specific assumptions about the underlying distribution.
p_Omnibus = circ_otest(angle_data)

%% Tests concerning mean and median
%%%%%%%% Confidence Intervals for the Mean Direction %%%%%%%%%%
%d = circ_confmean(angle_data, 0.01)
[alpha_bar ul ll] = circ_mean(angle_data)

%%%%%%%% One sample test for the mean angle %%%%%%%%%%%
% Similar to a one sample t-test on a linear scale,we can test 
% whether the population mean angle is equal to a specified direction.
p_mean = circ_mtest(angle_data,circ_ang2rad(180),0.001)