%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate anisotropy of D3 & D4 jumps near and away clusters 
% of bound molecules
%
% Note: input is txt files obtained from WriteTable and SeparateRegions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

clear all
close all
t = csvread('nuclear_SuH_6.txt');
ClusterTable = readtable('Res_knn_11_nuclear_SuH_6.txt');
name = 'Res_knn_11_nuclear_SuH_6_550nmSEARCH';
%%
t(:,7)= abs(t(:,5))>150 & abs(t(:,5))<210;

[mm sds ss ll md] = grpstats(t,t(:,1),{'mean','std', 'sum', 'length', 'mode'}); %grpstats based on trajid t(:,1)

idD1D2 = find(md(:,2)<3); % bound tracks
idD3D4 = find(md(:,2)>2); % free/exploratory tracks
%%
ClusterTable = ClusterTable(:,2:end);
ClusterTable = table2array(ClusterTable);
idClust = find(ClusterTable(:,4)==2);
%% plot D3 D4 with bound molecules (color-coded based on nnclean)
fig2 = figure;
scatter(mm(idD3D4,3), mm(idD3D4,4), '.c')
hold on
gscatter(ClusterTable(:,2), ClusterTable(:,3), ClusterTable(:,4)); hold on
legend off
set(gca, 'YDir','reverse')
axis equal
saveas(fig2,['AllBoundWithAllD3D4',name,'.fig'])
%% identify D3 D4 near clustered bound and plot
[clusterid dcenter]= knnsearch([ClusterTable(idClust,2), ClusterTable(idClust,3)], t(:,3:4), 'K',1);
ii = find((dcenter<550)&(t(:,2)>2));
iii = find((dcenter>=550)&(t(:,2)>2));
fig3 = figure;
scatter(ClusterTable(idClust,2), ClusterTable(idClust,3), '.y'); hold on
gscatter(t(ii,3), t(ii,4), clusterid(ii));
legend off
set(gca, 'YDir','reverse')
axis equal
saveas(fig3,['ClusteredBoundWithD3D4Near',name,'.fig'])
%%
AnisotropyAwayClust = sum(t(iii,7)==1)/length(iii);
AnisotropyNearClust = sum(t(ii,7)==1)/length(ii);
nAway = length(iii);
nNear = length(ii);

A = [AnisotropyAwayClust, AnisotropyNearClust, nAway, nNear];
T = array2table(A,...
    'VariableNames',{'AnisotropyAwayClust','AnisotropyNearClust', 'nAway','nNear'})
writetable(T,['AnisotropyNearAway',name,'.csv'])

