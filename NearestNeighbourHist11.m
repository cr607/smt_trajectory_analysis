%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot histogram of molecules' distance to 11th nearest neighbour
%
% Note: input is txt file obtained from WriteTable.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all

t = csvread('away_17_03_Notch_2.txt'); %input (obtained from SeparateRegions)
name = 'away_17_03_Notch_2';

[mm sds ss ll md] = grpstats(t,t(:,1),{'mean','std', 'sum', 'length', 'mode'}); %grpstats based on trajid t(:,1)
idD1D2 = find(md(:,2)<3); % bound tracks
[id d] = knnsearch(mm(idD1D2,3:4),mm(idD1D2,3:4),  'K', 17);
fig1 = figure;
hist(d(:,11), 1000); title('11')
xlim([0,2000])
saveas(fig1,['NearestNeighbourHist11',name,'.fig'])