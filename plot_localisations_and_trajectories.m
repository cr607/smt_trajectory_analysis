%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot localisations and trajectories based on vbSPT results

% Note: requires vbSPT function VB3_readData (Persson et al.,
% 2013)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all
global minTraj 
minTraj  = 2;

%make sure you are in the parent folder of the results folder
px = 110;    % pixel size
[fn1 pt1] = uigetfile('*.mat', 'Select vbSPT output file'); %vbSPT output
%%
[fn2 pt2] = uigetfile('*.tif', 'Select locus image');
im = imread(fullfile(pt2, fn2));
original_image = imread(fn2);
figure
imshow(original_image, []) %plot original image

%% read in vbspt result
a = load(fullfile(pt1, fn1));

% Read in the trajectory data
a.options.dim = 2;
D_states = a.Wbest.est.DdtMean/a.options.timestep;
X = VB3_readData(a.options);
traj_state = a.Wbest.est2.sMaxP;
start = 1;
stop = length(X);
longTrajs = [];

% Loop over trajectories longer than minTraj
trjColoring = '<d(t)>';
lineWidth=1;
lg = cellfun(@length, X);
qmode = cellfun(@mode, traj_state);
counts = cellfun(@length,traj_state);
qmode(counts<2) = [];

q = zeros(stop-start+1,5);
for i = start:stop
    q(i,:) = [X{i}(1,1),X{i}(1,2),X{i}(end,1)-X{i}(1,1),X{i}(end,2)-X{i}(1,2),double(size(X{i},1))];
end

%% detect locus
%image thresholding using Otsu’s method (multithresh)
%a 2-D Gaussian smoothing kernel is used for filtering (imgaussfilt)
sigma1 = 4; %standard deviation of 2-D Gaussian smoothing kernel  
threshold_no = 4;
threshold_used = 2;
 
t = multithresh(im-imgaussfilt(im,sigma1),threshold_no);
figure; imagesc(im-imgaussfilt(im,sigma1)>t(threshold_used))
plane = im-imgaussfilt(im,sigma1)>t(threshold_used);
[ly lx] = find(plane);

% Set locus coordinates
x_1 = 160; x_2 = 280; 
y_1 = 75; y_2 = 200;


% remove non-locus pixels
lx_keep_id = find(lx>x_1 & lx<x_2);
ly_keep_id = find(ly>y_1 & ly<y_2);
l_keep_id = intersect(lx_keep_id,ly_keep_id);

lx = lx(l_keep_id);
ly = ly(l_keep_id);

% plot locus
plane = logical(zeros(size(plane)));
for i = 1:length(ly)
    plane(ly(i),lx(i))=logical(1);
end
figure;imagesc(plane)

xlim([x_1,x_2]);
ylim([y_1,y_2]);

%% plot localisations
figure('Renderer', 'painters', 'Position', [600 600 310 344])
imshow(original_image, []) %plot original image
% imagesc(plane)
hold on
traj_id = cell(1,4);
state_fraction = zeros(1,4);
for i =1:4
    traj_id{i} = find(qmode==i);
    hold on
    for k = 1:length(traj_id{i}) 
        plot(X{traj_id{i}(k)}(:,1)/px,X{traj_id{i}(k)}(:,2)/px,'.','MarkerSize',4); hold on
    end 
end 

%% plot trajectories
figure('Renderer', 'painters', 'Position', [600 600 310 344])
imshow(original_image, []) %plot original image
% imagesc(plane)
hold on
traj_id = cell(1,4);
state_fraction = zeros(1,4);
for i =1:4
    traj_id{i} = find(qmode==i);
    hold on
    for k = 1:length(traj_id{i}) 
        plot(X{traj_id{i}(k)}(:,1)/px,X{traj_id{i}(k)}(:,2)/px,'LineWidth',0.6); hold on
    end 
end 

%% plot trajectories per state
figure
C = {[0 0.4470 0.7410],[0.6350 0.0780 0.1840],[0 0 0],[0.9290 0.6940 0.1250]};
% imshow(original_image, []) %plot original image
% imagesc(plane)
hold on
traj_id = cell(1,4);
state_fraction = zeros(1,4);
for i =4:-1:1
    traj_id{i} = find(qmode==i);
    hold on
    for k = 1:length(traj_id{i}) 
        plot(X{traj_id{i}(k)}(:,1)/px,X{traj_id{i}(k)}(:,2)/px,'Color',C{i},'LineWidth',0.8); hold on
    end 
end 
set(gca, 'YDir','reverse')
axis equal tight

