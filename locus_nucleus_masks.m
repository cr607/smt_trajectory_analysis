%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate locus:nuclear area

% Note: requires vbSPT function VB3_readData (Persson et al.,
% 2013)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all
clear all

px = 110;    %pixel size

sigma1 = 10; %standard deviation of 2-D Gaussian smoothing kernel  
threshold_no = 4;
threshold_used = 2;

x_1 = 305; x_2 = 360; %locus coordinates
y_1 = 100; y_2 = 140;

%% read in vbspt result
[fn1 pt1] = uigetfile('*.mat', 'Select vbSPT output file'); %vbSPT output
a = load(fullfile(pt1, fn1));

[fn2 pt2] = uigetfile('*.tif', 'Select locus image');
im = imread(fullfile(pt2, fn2));
original_image = imread(fn2);

% Read in the trajectory data
a.options.dim = 2;
D_states = a.Wbest.est.DdtMean/a.options.timestep;
X = VB3_readData(a.options);
traj_state = a.Wbest.est2.sMaxP;
start = 1;
stop = length(X);
longTrajs = [];

% Loop over trajectories longer than minTraj
trjColoring = '<d(t)>';
lineWidth=1;
lg = cellfun(@length, X);
qmode = cellfun(@mode, traj_state);
counts = cellfun(@length,traj_state);
qmode(counts<2) = [];

q = zeros(stop-start+1,5);
for i = start:stop
    q(i,:) = [X{i}(1,1),X{i}(1,2),X{i}(end,1)-X{i}(1,1),X{i}(end,2)-X{i}(1,2),double(size(X{i},1))];
end

%% plot trajectories
figure;
state_fraction = zeros(1,4);

h_im = imshow(original_image, [])
hold on

for k = 1:length(X) 
    plot(X{k}(:,1)/px,X{k}(:,2)/px); hold on
end 

%% create nucleus mask
e1 = impoly(gca,'Closed',true); 
mask1 = createMask(e1,h_im);

figure
imagesc(mask1)

%% detect locus
%image thresholding using Otsu�s method (multithresh)
%a 2-D Gaussian smoothing kernel is used for filtering (imgaussfilt)
t = multithresh(im-imgaussfilt(im,sigma1),threshold_no);
figure; imagesc(im-imgaussfilt(im,sigma1)>t(threshold_used))

figure
h_im = imshow(original_image, []) %plot original image

xlim([x_1,x_2]);
ylim([y_1,y_2]);
hold on

%% create locus mask
title('Click on vertices of a polygon to select locus')
e = impoly(gca,'Closed',true); 
mask = createMask(e,h_im);
  
dmask1 = imdilate(mask, strel('disk',5));
figure
imagesc(dmask1)

%% identify tracks near/away based on their starting position
d = bwdist(im-imgaussfilt(im,sigma1)>t(threshold_used)); %computes the Euclidean distance transform of the binary image 
%for each pixel, the distance transform assigns a number that is the distance between that pixel and the nearest
%nonzero pixel of the image.
startpos = [];
endpos = [];
sd = [];
for k = 1:size(q,1)
    startpos(k) = d(max(1,floor(q(k,2)/px)),max(1,floor(q(k,1)/px))); %note that the order of x and y must be reversed
    endpos(k) = d(max(1,floor((q(k,2)+q(k,4))/px)),max(1,floor((q(k,1)+q(k,3))/px)) );
    %signed distance 
        % - positive dist. motion away from locus
        % - negative dist. motion towards from locus
    sd(k) =endpos(k)-startpos(k);    
end

radius_size = [5];
id = {};
not_id = {};
for i = 1:length(radius_size)
    id{i} = find(startpos <= radius_size(i)|endpos <= radius_size(i)); %near locus
    
    trajectory_id = [1:size(q,1)];
    idx = ismember(trajectory_id, id{i});
    not_id{i} = trajectory_id(~idx); %away from locus
end

%% plot near locus trajectories
figure; 
track_length = cell(1,length(radius_size));

for i = 1:length(radius_size)
    track_length{i} = zeros(1,length(id{i}));  
    subplot(1,length(radius_size),i)
    imagesc(dmask1)
    hold on
    for k = 1:length(id{i})    
        track_length{i}(k) = length(X{id{i}(k)}(:,1));
        plot(X{id{i}(k)}(:,1)/px,X{id{i}(k)}(:,2)/px, '-'); hold on
    end
    title({['Near locus trajectories ',sprintf('%d',radius_size(i)),'px']});
    hold off
end

area_nucleus = size(find(mask1==1),1)
area_locus = size(find(dmask1==1),1)

locus_to_nucleus_ratio = area_locus/area_nucleus