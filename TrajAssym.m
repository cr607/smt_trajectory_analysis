%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate trajectory assymmetry
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [a2 A2 ]= TrajAssym(x)
    global minTraj  
    if(size(x,1) >= minTraj)
        Txx = mean(x(:,1).^2)-mean(x(:,1))^2;
        Tyy = mean(x(:,2).^2)-mean(x(:,2))^2;
        Txy = mean(x(:,1).*x(:,2))-mean(x(:,1))*mean(x(:,2));
        R1 =  (1/2*(Txx+Tyy+sqrt((Txx-Tyy)^2+4*Txy^2)));
        R2 =  (1/2*(Txx+Tyy-sqrt((Txx-Tyy)^2+4*Txy^2)));
        a2 = R2/R1;
        A2 = (R1-R2).^2./(R1+R2).^2;
    else
        a2 = nan;
        A2 = nan;
    end
end