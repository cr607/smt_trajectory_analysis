%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot trajectory density maps based on vbSPT (Persson et al. 2013) 
% results 
%
% Note: requires brewermap function
% Stephen23 (2023). ColorBrewer: Attractive and Distinctive Colormaps 
% (https://github.com/DrosteEffect/BrewerMap/releases/tag/3.2.3), GitHub.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global minTraj 
minTraj  = 4;
nrstates = 4
%% make sure you are in the parent folder of the results folder
px = 110    % pixel size
[fn pt] = uigetfile('*.mat', 'Select vbspt mat file');

[fn2 pt2] = uigetfile('*.tif', 'Select locus image');
im = imread(fullfile(pt2, fn2));
%im = []

sigma1 = 4; %standard deviation of 2-D Gaussian smoothing kernel  
threshold_no = 2;
threshold_used = 2;
%% Detect locus, then xlim and ylim accordingly
x_1 = 160; x_2 = 280; 
y_1 = 75; y_2 = 200;


t = multithresh(im-imgaussfilt(im,sigma1),threshold_no);
figure; imagesc(im-imgaussfilt(im,sigma1)>t(threshold_used))
mask = im-imgaussfilt(im,sigma1)>t(threshold_used);
[ly lx] = find(mask);

% remove non-locus pixels
lx_keep_id = find(lx>x_1 & lx<x_2);
ly_keep_id = find(ly>y_1 & ly<y_2);
l_keep_id = intersect(lx_keep_id,ly_keep_id);
lx = lx(l_keep_id);
ly = ly(l_keep_id);

mask = logical(zeros(size(mask)));
for i = 1:length(ly)
    mask(ly(i),lx(i))=logical(1);
end
[yyLoc xxLoc] = ind2sub(size(mask),find(bwperim(mask)>0));


%% read in vbspt result
a = load(fullfile(pt, fn));

% Read in D for the found states
D_states = a.Wbest.est.DdtMean/a.options.timestep;
D2 = a.Wbest.est2.viterbi;
% Read in the trajectory data
a.options.dim = 2;
X = VB3_readData(a.options);
%%
traj_state = a.Wbest.est2.sMaxP;
start = 1;
stop = length(X);
longTrajs = [];
% Loop over trajectories longer than minTraj
trjColoring = '<d(t)>'
lineWidth=1;
%% thresholding the locus image
t = multithresh(im-imgaussfilt(im,sigma1),threshold_no);
locus = (im-imgaussfilt(im,sigma1))>t(threshold_used);
figure; imagesc(locus); title('Locus')
% locus pixels
[ly lx] = find((im-imgaussfilt(im,sigma1))>t(threshold_used));
% compute distance to locus
d = bwdist((im-imgaussfilt(im,sigma1))>t(threshold_used)); % title('Distance to locus')
figure; imagesc(d); title('Distance from closest locus pixel')

%% compute quiver data

q = zeros(stop-start+1,5);
for i = start:stop
    q(i,:) = [X{i}(1,1)/px,X{i}(1,2)/px,X{i}(end,1)/px-X{i}(1,1)/px,X{i}(end,2)/px-X{i}(1,2)/px,double(size(X{i},1))];
end

lg = cellfun(@length, X);
%qc = cellfun(@median,traj_state);
qmode = cellfun(@mode, traj_state);
counts = cellfun(@length,traj_state);
qmode(counts<2) = [];

%% Density - localisations (possible tracks)

step = 100;

xx = cell2mat(X');
gridx1 = min(xx(:,1)):step:max(xx(:,1));
gridx2 = min(xx(:,2)):step:max(xx(:,2));
[x1,x2] = meshgrid(gridx1, gridx2);
xi = [x1(:) x2(:)];

figure
[ans1,ans2,bw] = ksdensity(q(:,1:2),xi/px); title('Density map - all trajectories')

[ans1,ans2,bw] = ksdensity(q(:,1:2),xi/px,'Bandwidth',4); title('Density map - all trajectories')
surf(reshape(ans1, size(x1))); hold on; title('Density map - all trajectories')
 
hold on
xxLoc_new = (xxLoc*110 - min(xx(:,1)))/step + 1;
yyLoc_new = (yyLoc*110 - min(xx(:,2)))/step + 1;
loc1 = plot3(xxLoc_new,yyLoc_new,repmat(max(ans1),length(xxLoc),1),'.','Color',[0.9290 0.6940 0.1250],'MarkerSize',3,'DisplayName','Locus');

shading interp
set(gca, 'YDir','reverse')
% clim([0 0.00003])
axis equal
colormap(brewermap([],"Greys"))
view(2)

[ymax xmax] = size(x1);
xlim([0,xmax])
ylim([0,ymax])
colorbar
% clim([0, 0.1387*1.0e-03])
clim("auto")
axis equal


%%

for s = max(unique(qmode)):-1:1
    id = find((qmode==s));
    kd{s} =  ksdensity(q(id,1:2),xi/px,'Bandwidth',4);     
end

%% density maps

PlotSize = size(x1);

figure;
% subplot(221)
surf(reshape(kd{1}, size(x1))); hold on; title('D1')
shading interp
clim([0 0.00013])
hold on
% loc1 = plot3(xxLoc_new,yyLoc_new,repmat(max(kd{1}),length(xxLoc),1),'.w','MarkerSize',3,'DisplayName','Locus');
loc1 = plot3(xxLoc_new,yyLoc_new,repmat(max(kd{1}),length(xxLoc),1),'.','Color',[0.9290 0.6940 0.1250],'MarkerSize',3,'DisplayName','Locus');

set(gca, 'YDir','reverse')
colormap(brewermap([],"Greys"))
view(2)
xlim([0,PlotSize(2)])
ylim([0,PlotSize(1)])
colorbar
axis equal

figure;
surf(reshape(kd{2}, size(x1))); hold on; title('D2')
shading interp
clim([0 0.00013])
hold on
% loc1 = plot3(xxLoc_new,yyLoc_new,repmat(max(kd{2}),length(xxLoc),1),'.w','MarkerSize',3,'DisplayName','Locus');
loc1 = plot3(xxLoc_new,yyLoc_new,repmat(max(kd{2}),length(xxLoc),1),'.','Color',[0.9290 0.6940 0.1250],'MarkerSize',3,'DisplayName','Locus');

set(gca, 'YDir','reverse')
colormap(brewermap([],"Greys"))
view(2)
xlim([0,PlotSize(2)])
ylim([0,PlotSize(1)])
colorbar
axis equal

figure;
surf(reshape(kd{3}, size(x1))); hold on; title('D3')
shading interp
clim([0 0.00013])
hold on
% loc1 = plot3(xxLoc_new,yyLoc_new,repmat(max(kd{3}),length(xxLoc),1),'.w','MarkerSize',3,'DisplayName','Locus');
loc1 = plot3(xxLoc_new,yyLoc_new,repmat(max(kd{3}),length(xxLoc),1),'.','Color',[0.9290 0.6940 0.1250],'MarkerSize',3,'DisplayName','Locus');

set(gca, 'YDir','reverse')
colormap(brewermap([],"Greys"))
view(2)
xlim([0,PlotSize(2)])
ylim([0,PlotSize(1)])
colorbar
axis equal

figure;
surf(reshape(kd{4}, size(x1))); hold on; title('D4')
shading interp
clim([0 1.0e-03 * 0.2639])
hold on
% loc1 = plot3(xxLoc_new,yyLoc_new,repmat(max(kd{4}),length(xxLoc),1),'.w','MarkerSize',3,'DisplayName','Locus');
loc1 = plot3(xxLoc_new,yyLoc_new,repmat(max(kd{4}),length(xxLoc),1),'.','Color',[0.9290 0.6940 0.1250],'MarkerSize',3,'DisplayName','Locus');

set(gca, 'YDir','reverse')
colormap(brewermap([],"Greys"))
view(2)
xlim([0,PlotSize(2)])
ylim([0,PlotSize(1)])
colorbar
axis equal