%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate proportion of bound molecules that are clustered
%
% Note: input is txt files obtained from WriteTable and SeparateRegions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%% 
clear all
close all
t = csvread('nuclear_SuH_6.txt');
ClusterTable = readtable('Res_knn_11_nuclear_SuH_6.txt');
name = 'Res_knn_11_nuclear_SuH_6_550nmSEARCH';
%%
t(:,7)= abs(t(:,5))>150 & abs(t(:,5))<210;

[mm sds ss ll md] = grpstats(t,t(:,1),{'mean','std', 'sum', 'length', 'mode'}); %grpstats based on trajid t(:,1)

idD1D2 = find(md(:,2)<3); % bound tracks
idD3D4 = find(md(:,2)>2); % free/exploratory tracks
%%
ClusterTable = ClusterTable(:,2:end);
ClusterTable = table2array(ClusterTable);
idClust = find(ClusterTable(:,4)==2);

%% identify D3 D4 near clustered bound and plot
[clusterid dcenter]= knnsearch([ClusterTable(idClust,2), ClusterTable(idClust,3)], t(:,3:4), 'K',1);
ii = find((dcenter<550)&(t(:,2)>2));
iv = find(t(:,2)>2);

Proportion_bound_in_clusters = length(idClust)/length(idD1D2)

