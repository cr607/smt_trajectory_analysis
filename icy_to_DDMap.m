%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Convert Icy (Chenouard, Bloch, and Olivo-Marin 2013) 
% output to DDMap input allowing no gaps in tracking
%
% DDMap citation:
% Briane, Vincent, Charles Kervrann, and Myriam Vimond. 2018. 
% 'Statistical Analysis of Particle Trajectories in Living Cells'. 
% Physical Review E 97 (6): 062121. 
% Salomon, Antoine, Cesar Augusto Valades-Cruz, Ludovic Leconte, 
% and Charles Kervrann. 2020. ‘Dense Mapping of Intracellular Diffusion
% and Drift from Single-Particle Tracking Data Analysis’. 
% In ICASSP 2020 - 2020 IEEE International Conference on Acoustics, 
% Speech and Signal Processing (ICASSP), 966–70.
% 
% DDMap scripts are not yet published
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


no_blocks = 2;%in case movie was too large and had to be chopped in different blocks
px = 110; %vbSPT reads in nm
size_table = 0;
provideTRACKID = 0;

nuc = [];
[fnnuc ptnuc ] = uigetfile('*.tif*', 'Select nucleus file');
try
    nuc = imread(fullfile(ptnuc,fnnuc));
catch
    warning('No nucleus image available.');
end

if provideTRACKID == 1
  [fn1 pt1]= uigetfile('*.mat', 'Select trackid file')
  load(fn1)
end

[fn pt]= uigetfile('*.xml', 'Select xml file')


% read as an Excel file
filename = fullfile(pt,fn);
fileID=fopen(filename);

for ind=1:4,
    tline = fgetl(fileID);
end
    
clear Cell_data
ind_c=1;
ind_traj=1;
Cell_data=cell(400000,6);

while feof(fileID)~=1,
      for i=1:2,
          tline = fgetl(fileID);
      end
          
      while isequal(tline,'</track>')~=1 && feof(fileID)~=1,
            Cell_data{ind_c,1}=ind_traj;
            vec=textscan(tline,'%*s %*s %*s %*3s %f %*1s  %*6s %f %*1s %*3s %f  %*1s %*3s %f %*s %*3s %f %*1s','Delimiter',' ');
            Cell_data(ind_c,2:6)={vec{1},vec{2},vec{3},vec{4}, vec{5}};
            ind_c=ind_c+1;
            tline=fgetl(fileID);
      end
      ind_traj=ind_traj+1;
end

Cell_data((ind_c-1):end,:)=[];
mat=cell2mat(Cell_data);
table=array2table(mat,'VariableNames',{'Trajectory','Time','blink','x','y','z',});
size_table = size(table);
size_table = size_table(1);

fclose(fileID);

if no_blocks > 1
clear fn pt filename fileID tline mat ind_c vec Cell_data i ind
for block = 2 : no_blocks
    [fn pt]= uigetfile('*.xml', 'Select xml file')
    % read as an Excel file
    filename = fullfile(pt,fn);
    fileID=fopen(filename);

    for ind=1:4,
        tline = fgetl(fileID);
    end
    
    clear Cell_data
    ind_c=1;
    Cell_data=cell(400000,6);

    while feof(fileID)~=1,
          for i=1:2,
              tline = fgetl(fileID);
          end
          
          while isequal(tline,'</track>')~=1 && feof(fileID)~=1,
                Cell_data{ind_c,1}=ind_traj;
                vec=textscan(tline,'%*s %*s %*s %*3s %f %*1s  %*6s %f %*1s %*3s %f  %*1s %*3s %f %*s %*3s %f %*1s','Delimiter',' ');
                Cell_data(ind_c,2:6)={vec{1},vec{2},vec{3},vec{4}, vec{5}};
                ind_c=ind_c+1;
                tline=fgetl(fileID);
          end
          ind_traj=ind_traj+1;
    end

Cell_data((ind_c-1):end,:)=[];
mat=cell2mat(Cell_data);

table{size_table+1:size_table+length(mat),:} = mat;

size_table = size(table);
size_table = size_table(1);

fclose(fileID);

clear filename fileID tline mat ind_c vec Cell_data i ind
end
end
%% crop data
if provideTRACKID == 0
   trackid = SelectPointsXml_new(table2array(table), nuc);
   save(fullfile(pt, strcat('trackid_',strtok(fn, '.'), '.mat')), 'trackid');
end
%% create tracks and chopped tracks
clear track chtrack
% chopped tracks are the tracks that do not allow blinks
id = unique(table.Trajectory);
ct0 = 1;
ct = 1;
for k = 1:length(id)
    if ismember(id(k), trackid)
    ii = find(table.Trajectory==id(k));
    track{ct0} = [table.x(ii) table.y(ii) table.Time(ii)];
    chop = [0; find(table.blink(ii)==2); size(track{ct0},1)+1];
    if length(chop) ==2
        if size(track{ct0},1)>2
            chtrack{ct} =  track{ct0}(:,1:3); 
            ct = ct+1;
        end
    else
    for m =2:length(chop)
        if (chop(m)-chop(m-1))>3
            chtrack{ct} = [table.x(ii(chop(m-1)+1:chop(m)-1)) table.y(ii(chop(m-1)+1:chop(m)-1)) ...
                           table.Time(ii(chop(m-1)+1:chop(m)-1))];
            ct = ct+1;
        end
        
    end
    end
    ct0 =ct0+1;
    end
end
clear x y
currentLength = 0;
for i = 1 : size(chtrack,2)
    Trajectory(currentLength+1:currentLength+size(chtrack{i},1),1) = repmat(i,size(chtrack{i},1),1);
    Time(currentLength+1:currentLength+size(chtrack{i},1),1) = chtrack{i}(:,3);
    x(currentLength+1:currentLength+size(chtrack{i},1),1) = chtrack{i}(:,1);
    y(currentLength+1:currentLength+size(chtrack{i},1),1) = chtrack{i}(:,2);
    currentLength = length(Trajectory);
end
clear table

T = table(Trajectory,Time,x,y);
save(fullfile(pt, strcat('DDMap_',strtok(fn, '.'), '.mat')), 'T');