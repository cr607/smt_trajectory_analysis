%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prepare Thunderstorm (Ovesný et al. 2014) outputs for Icy (Chenouard,
% Bloch, and Olivo-Marin 2013)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc
clear
block = 2000 % frames
px_size = 110
name_main_dir= uigetdir('','Select folder with Thuderstorm localisations')
files=dir([name_main_dir '\*TS.csv']);
%%

for n_files=1:length(files)
    n_files
    files(n_files).name
    data_wd=importdata(fullfile(name_main_dir, files(n_files).name));
    data_wd=data_wd.data;
    
    data_icy=[data_wd(:,1) data_wd(:,2)/px_size data_wd(:,3)/px_size zeros(length(data_wd(:,1)),1) data_wd(:,5) data_wd(:,5) data_wd(:,5)];
    
    for d = min(data_wd(:,1)):block:max(data_wd(:,1)) 
        id = find((data_wd(:,1)>=d)&(data_wd(:,1)<d+block));
        dlmwrite(fullfile(name_main_dir, ['icy_' files(n_files).name(1:end-4) num2str(d) '.txt']),data_icy(id,:),' ');    
    end

end