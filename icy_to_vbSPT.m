%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Convert Icy (Chenouard, Bloch, and Olivo-Marin 2013) output 
% to chopped_track for vbSPT (Persson et al. 2013) input allowing 
% no gaps in tracking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


no_blocks = 1; %in case movie was too large and had to be chopped in different blocks
px = 110; %vbSPT reads in nm
size_table = 0;

nuc = [];
[fnnuc ptnuc ] = uigetfile('*.*', 'Select nucleus file');
try
    nuc = imread(fullfile(ptnuc,fnnuc));
catch
    warning('No nucleus image available.');
end

[fn pt]= uigetfile('*.xml', 'Select xml file')


% read as an Excel file
filename = fullfile(pt,fn);
fileID=fopen(filename);

for ind=1:4,
    tline = fgetl(fileID);
end
    
clear Cell_data
ind_c=1;
ind_traj=1;
Cell_data=cell(400000,6);

while feof(fileID)~=1,
      for i=1:2,
          tline = fgetl(fileID);
      end
          
      while isequal(tline,'</track>')~=1 && feof(fileID)~=1,
            Cell_data{ind_c,1}=ind_traj;
            vec=textscan(tline,'%*s %*s %*s %*3s %f %*1s  %*6s %f %*1s %*3s %f  %*1s %*3s %f %*s %*3s %f %*1s','Delimiter',' ');
            Cell_data(ind_c,2:6)={vec{1},vec{2},vec{3},vec{4}, vec{5}};
            ind_c=ind_c+1;
            tline=fgetl(fileID);
      end
      ind_traj=ind_traj+1;
end

Cell_data((ind_c-1):end,:)=[];
mat=cell2mat(Cell_data);
table=array2table(mat,'VariableNames',{'Trajectory','Time','blink','x','y','z',});
size_table = size(table);
size_table = size_table(1);

fclose(fileID);

if no_blocks > 1
clear fn pt filename fileID tline mat ind_c vec Cell_data i ind
for block = 2 : no_blocks
    [fn pt]= uigetfile('*.xml', 'Select xml file')
    % read as an Excel file
    filename = fullfile(pt,fn);
    fileID=fopen(filename);

    for ind=1:4,
        tline = fgetl(fileID);
    end
    
    clear Cell_data
    ind_c=1;
    Cell_data=cell(400000,6);

    while feof(fileID)~=1,
          for i=1:2,
              tline = fgetl(fileID);
          end
          
          while isequal(tline,'</track>')~=1 && feof(fileID)~=1,
                Cell_data{ind_c,1}=ind_traj;
                vec=textscan(tline,'%*s %*s %*s %*3s %f %*1s  %*6s %f %*1s %*3s %f  %*1s %*3s %f %*s %*3s %f %*1s','Delimiter',' ');
                Cell_data(ind_c,2:6)={vec{1},vec{2},vec{3},vec{4}, vec{5}};
                ind_c=ind_c+1;
                tline=fgetl(fileID);
          end
          ind_traj=ind_traj+1;
    end

Cell_data((ind_c-1):end,:)=[];
mat=cell2mat(Cell_data);

table{size_table+1:size_table+length(mat),:} = mat;

size_table = size(table);
size_table = size_table(1);

fclose(fileID);

clear filename fileID tline mat ind_c vec Cell_data i ind
end
end
%% crop data

trackid = SelectPointsXml(table2array(table), nuc);

%% create tracks and chopped tracks
 clear track chtrack
% chopped tracks are the tracks that do not allow blinks
id = unique(table.Trajectory);
ct0 = 1;
ct = 1;
for k = 1:length(id)
    if ismember(id(k), trackid)
    ii = find(table.Trajectory==id(k));
    track{ct0} = [table.x(ii) table.y(ii)];
    track{ct0}(:,1:2) = track{ct0}(:,1:2)*px;
    chop = [0; find(table.blink(ii)==2); size(track{ct0},1)+1];
    if length(chop) ==2
        if size(track{ct0},1)>2
            chtrack{ct} =  track{ct0}(:,1:2); 
            ct = ct+1;
        end
    else
    for m =2:length(chop)
        if (chop(m)-chop(m-1))>3
            chtrack{ct} = [table.x(ii(chop(m-1)+1:chop(m)-1))*px table.y(ii(chop(m-1)+1:chop(m)-1))*px ];%table.blink(ii(chop(m-1)+1:chop(m)-1))];
            ct = ct+1;
        end
        
    end
    end
    ct0 =ct0+1;
    end
end
%%
save(fullfile(pt, strcat('track',strtok(fn, '.'), '.mat')), 'track');
save(fullfile(pt, strcat('chopped_track',strtok(fn, '.'), '.mat')), 'chtrack');
%%
figure; plot(table.x(:)*px, table.y(:)*px, '.'); hold on
for k = 1:length(track)
    plot(track{k}(:,1),track{k}(:,2), 'r.'); hold on
end